var mongoose = require('mongoose')
// var mongoosePaginate = require('mongoose-paginate')


var ToDoSchema = new mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    title: String,
    description: String,
    date: Date,
    status: String,
    category: String
})

// ToDoSchema.plugin(mongoosePaginate)
const ToDo = mongoose.model('Todo', ToDoSchema)

module.exports = ToDo;