var express = require('express')
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

var router = express.Router()



var ToDoController = require('../controllers/todos');


// pentru fiecare functie trebuie sa avem o adresa

router.get('/', auth, ToDoController.getTodos)

router.post('/', auth, ToDoController.createTodo)

router.put('/', auth, ToDoController.updateTodo)

router.delete('/:id', auth ,ToDoController.removeTodo)


// exportam routerul

module.exports = router;