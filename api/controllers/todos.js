// Accessing the Service that we just created

var TodoService = require('../services/todos')

// salvam contextul controllerului intr-o variabila separata

_this = this


// functie async pentru a obtine lista

exports.getTodos = async function(req, res, next){

    // verificam daca exista parametri in request si daca nu exista, atunci ii asignam o valoare default
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10;

    try{

        var todos = await TodoService.getTodos({ userId: req.payload._id }, page, limit)

        // returnam lista si statusul HTTP care i se potriveste

        return res.status(200).json({status: 200, data: todos, message: "Succesfully Todos Recieved"});

    }catch(e){

        // cod de eroare

        return res.status(400).json({status: 400, message: e.message});

    }
}

exports.createTodo = async function(req, res, next){

    // Req.Body contine valorile din formularul de pe pagina frontend

    var todo = {
        userId: req.payload._id,
        title: req.body.title,
        description: req.body.description,
        status: req.body.status,
        date: req.body.date,
        category: req.body.category
    }

    try{

        // chemam serviciul cu obiectul obtinut din corpul requestului


        var createdTodo = await TodoService.createTodo(todo)
        return res.status(201).json({status: 201, data: createdTodo, message: "Succesfully Created ToDo"})
    }catch(e){


        return res.status(400).json({status: 400, message: "Todo Creation was Unsuccesfull"})
    }
}

exports.updateTodo = async function(req, res, next){

    // pentru a face un update, dar si pentru a sterge este necesar sa avem id-ul pentru todo item

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var todo = {
        id,
        userId: req.payload._id,
        title: req.body.title ? req.body.title : null,
        description: req.body.description ? req.body.description : null,
        status: req.body.status ? req.body.status : null,
        date: req.body.date ? req.body.date : null,
        category: req.body.category ? req.body.category : null
    }

    try{
        var updatedTodo = await TodoService.updateTodo(todo)
        return res.status(200).json({status: 200, data: updatedTodo, message: "Succesfully Updated Tod"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeTodo = async function(req, res, next){
    console.log('delete',id);
    var id = req.params.id;

    try{
        var deleted = await TodoService.deleteTodo(id)
        return res.status(204).json({status:204, message: "Succesfully Todo Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}
