# todoapp

Aceasta aplicatie este formata din doua parti: partea de backend si partea de frontend, ce trebuie instalate separat. Partea de backend este independenta de partea de frontend.

Initial, pentru a putea instala aplicatia, trebuie sa clonati acest folder.

  git clone git@bitbucket.org:ananescu/todoapp.git

Urmeaza instalarea partii de backend:

1. Aveti nevoie de MongoDb: https://docs.mongodb.com/manual/installation/ si este necesar ca acesta sa ruleze la adresa 127.0.0.1:27017
2. Aveti nevoie de Node.js. Atentie! Deoarece am folosit functii de tip async, este nevoie de o versiune mai mare sau egala cu 7.6: https://nodejs.org/en/
3. Miscellaneous: ar mai fi nevoie de npm: https://docs.npmjs.com/getting-started/installing-node si, optional de nvm

Pasi pentru instalarea partii de backend:

Dupa ce v-ati asigurat ca aveti toate uneltele necesare trebuie sa urmati pasii:

    cd todoapp -- pentru a intra in directorul aplicatiei
  
    npm install
  
    npm install -g nodemon
  
    npm start

Pentru instalarea partii de frontend:

    cd todoapp-angular -- pentru a intra in aplicatia frontend
  
    npm install -- pentru a rula dependintele
  
    npm install -g @angular/cli -- pentru a instala AngularJs global
  
    ng serve sau npm install pentru a rula serverul


Pentru ca aplicatia sa ruleze de fiecare data este necesar ca:

1. In orice folder in care sunteti situat, sa rulati sudo mongod (pornim baza de date, aceasta comanda s-ar putea sa difere in functie de sistemul de operare)
2. In folderul todoapp sa rulati npm start (pornim serverul pentru aplicatia backend) sau nodemon (acesta va face refresh automat pentru orice schimbari din cod)
3. In folderul client sa rulati ng serve sau npm install (pornim aplicatia frontend)

ATENTIE! Este necesar ca baza de date sa fie pornita inaintea serverului pentru aplicatia backend, pentru a se stabili o conexiune.
