class ToDo {
    _id:string;
    title: string;
    description: string;
    date: Date;
    status: string;
    category: string;

    constructor(
    ){
        this.title = ""
        this.description = ""
        this.date = new Date()
        this.status = ""
        this.category = ""
    }
}

export default ToDo;
