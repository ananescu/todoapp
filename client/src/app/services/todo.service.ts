import ToDo from '../models/todo.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../authentication.service';
import { Router } from '@angular/router';

//RxJS operator for mapping the observable
import 'rxjs/add/operator/map';

@Injectable()
export class TodoService {

  api_url = '/api/';
  todoUrl = `/api/todos`;

  credentials: TokenPayload = {
    email: '',
    password: ''
  };



  constructor(private auth: AuthenticationService, private router: Router, private http: HttpClient) {console.log('test')}

  //Create todo, takes a ToDo Object
  createTodo(todo: ToDo): Observable<any>{
    //returns the observable of http post request
    return this.http.post(`${this.todoUrl}`, todo, { headers: { Authorization: `Bearer ${this.auth.returnToken()}` }});
  }

  //Read todo, takes no arguments
  
  getToDos(){
    console.log(this.todoUrl) 
    return this.http.get(this.todoUrl, { headers: { Authorization: `Bearer ${this.auth.returnToken()}` }})
    .map(res  => {
      return res;
    }
    ) ; 
    
  }
  //Update todo, takes a ToDo Object as parameter
  editTodo(todo:ToDo){
    let editUrl = `${this.todoUrl}`
    //returns the observable of http put request
    return this.http.put(editUrl, todo, { headers: { Authorization: `Bearer ${this.auth.returnToken()}` }});
  }

  deleteTodo(id:string):any{
    //Delete the object by the id
    let deleteUrl = `${this.todoUrl}/${id}`
    return this.http.delete(deleteUrl, { headers: { Authorization: `Bearer ${this.auth.returnToken()}` }})
    .map(res  => {
      return res;
    })
  }

  //Default Error handling method.
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
